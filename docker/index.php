<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
        <?php
        echo "<h2>Docker là gì?</h2>";
        ?>
        <p>
        Docker là một nền tảng được thiết kế ra nhằm hỗ trợ các nhà phát triển trong việc tạo, triển khai, chạy ứng dụng bằng cách dùng container. Container ở đây là các môi trường tách biệt, độc lập được tạo ra bởi nền tảng Docker.
        Với sự hỗ trợ của nền tảng này, nhà phát triển có nhu cầu deploy lên server chỉ cần run container được tạo bởi Docker, và application sẽ lập tức khởi chạy. Đơn giản, nhanh chóng và dễ sử dụng là những ưu điểm khá nổi bật mà Docker mang lại.
        </p>
</body>
</html>